// module.exports = {
// 	e2e: {
// 		baseUrl: 'localhost:3000',
// 		specPattern: 'cypress/integration/*.spec.{js,jsx,ts,tsx}',
// 		setupNodeEvents(on, config) {
// 			// implement node event listeners here
// 		},
// 	},
// }


module.exports = {
  e2e: {
    baseUrl: 'http://127.0.0.1:38779',
	//specPattern: 'cypress/integration/*.spec.{js,jsx,ts,tsx}',
  specPattern: 'cypress/e2e/test-trajet-hackathon.cy.js',
	setupNodeEvents(on, config) {
  },
},
}
