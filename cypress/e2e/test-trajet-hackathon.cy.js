describe('Recherche dun trajet Paris-Bruxelles pour la date du jour.', () => {
    it('Recherche un trajet pour la date du jour', () => {
        cy.visit("http://127.0.0.1:38779");
        cy.get('#departure').type('paris')
        cy.get('#arrival').type('bruxelles')
        cy.get('#search').click()
        cy.get('#results > :nth-child(1)').contains('Paris')
    });
  
});