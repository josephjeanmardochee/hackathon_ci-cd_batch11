var express = require("express");
//var mongoose = require("mongoose");

var app = express();

//environnement variables
require('dotenv').config();

// //database connection
// const uri = process.env.CONNECTION_STRING;
// mongoose.connect(uri,{useNewUrlParser:true,useCreateIndex:true});
// const connection = mongoose.connection;
// connection.once('open', () => {
// 	console.log("Connected Database Successfully");
// });

app.listen(8484,function(req, res){
	console.log("Server is started on port 8484");
})

var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var cartRouter = require('./routes/cart');
var bookingsRouter = require('./routes/bookings');

var app = express();

const cors = require('cors');
app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/cart', cartRouter);
app.use('/bookings', bookingsRouter);

module.exports = app;
